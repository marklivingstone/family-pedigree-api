from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Member

class MemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = [
            'id',
            'user_id',
            'first_name',
            'last_name',
            'relation',
            'vital_status'
        ]