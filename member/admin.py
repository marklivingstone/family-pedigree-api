from django.contrib import admin
from .models import Member

# Register your models here.

class MemberAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'first_name', 'last_name', 'relation', 'vital_status')
    search_fields = ('user_id' ,'first_name', 'last_name')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Member, MemberAdmin)
