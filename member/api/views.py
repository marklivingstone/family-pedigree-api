from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from account.models import Account
from member.models import Member

from member.api.serializers import MemberSerializer

@api_view(['GET', ])
def api_get_members(request, pk):

    try:
        member = Member.objects.filter(user_id=pk).order_by("relation")
    except Account.DoesNotExist:
        return Response("Account doesn't exist", status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = MemberSerializer(member, many=True)
        return Response(serializer.data)

@api_view(['GET', ])
def api_get_member(request, pk):

    try:
        member = Member.objects.get(id=pk)
    except Account.DoesNotExist:
        return Response("Account doesn't exist", status=status.HTTP_404_NOT_FOUND)

    user_id = request.user.id

    if member.user_id != user_id:
        return Response({"You're not allowed to view this."}, status=status.HTTP_401_UNAUTHORIZED)

    if request.method == "GET":
        serializer = MemberSerializer(member)
        return Response(serializer.data)

@api_view(['POST', ])
def api_add_member(request, pk):

    if request.method == 'POST':

        data = request.data
        user_id = pk
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        dob = data.get('dob')
        relation = data.get('relation')
        vital_status = data.get('vital_status')

        serializer = MemberSerializer(data=data)
        data = {}
        if serializer.is_valid():
            member = Member.objects.create(user_id=user_id, first_name=first_name, last_name=last_name,
                                           dob=dob, relation=relation, vital_status=vital_status)
            data['response'] = "Member has been successfully added."
            data['first_name'] = member.first_name
            data['last_name'] = member.last_name
        else:
            return Response("Failed to add new family member", status=status.HTTP_400_BAD_REQUEST)
        return Response(data)

@api_view(['PUT', ])
def api_update_member(request, pk):

    try:
        member = Member.objects.get(id=pk)
    except Account.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_id = request.user.id

    if member.user_id != user_id:
        return Response({"You're not allowed to update this."}, status=status.HTTP_401_UNAUTHORIZED)

    if request.method == "PUT":
        serializer = MemberSerializer(member, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE', ])
def api_remove_member(request, pk):

    try:
        member = Member.objects.get(id=pk)
        account = Account.objects.get(id=pk)
    except Member.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_id = request.user.id

    if member.user_id != user_id:
        return Response({"You're not allowed to delete this data."}, status=status.HTTP_401_UNAUTHORIZED)

    if member.user_id == user_id and account.id == user_id:
        return Response({"You're not allowed to delete your own data."}, status=status.HTTP_403_FORBIDDEN)

    if request.method == "DELETE":
        operation = member.delete()
        data = {}

        if operation:
            return Response("Data has been successfully removed.", status=status.HTTP_200_OK)
        else:
            return Response("Failed to remove data.", status=status.HTTP_400_BAD_REQUEST)