from django.urls import path
from member.api.views import (
    api_add_member,
    api_get_members,
    api_get_member,
    api_update_member,
    api_remove_member
)

app_name = 'member'

urlpatterns = [
    path('<int:pk>/', api_get_member, name="member"),
    path('<int:pk>/members/', api_get_members, name="members"),
    path('<int:pk>/add_new/', api_add_member, name="add"),
    path('update/<int:pk>/', api_update_member, name="update"),
    path('remove/<int:pk>/', api_remove_member, name="remove")
]