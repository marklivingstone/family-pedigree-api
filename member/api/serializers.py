from django.contrib.auth.models import User
from rest_framework import serializers

from member.models import Member


class MemberSerializer(serializers.ModelSerializer):

    class Meta:
        model = Member
        fields = [
            'id',
            'user_id',
            'first_name',
            'last_name',
            'dob',
            'relation',
            'vital_status'
        ]