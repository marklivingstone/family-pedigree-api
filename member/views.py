from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.viewsets import ViewSet

import requests

from .models import *
from .serializers import *

# Create your views here.


class MemberViewSet(ViewSet):

    @action(methods=['POST'], detail=False)
    def add_member(self, request, *args, **kwargs):
        data = request.data
        user_id = data.get('user_id')
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        dob = data.get('dob')
        relation = data.get('relation')
        vital_status = data.get('vital_status')

        m_serializer = MemberSerializer(data)

        user = User.objects.filter(id=user_id).first()

        if not user:
            return Response(m_serializer.errors)

        member = Member.objects.create(user=user, first_name=first_name, last_name=last_name,
                                       dob=dob, relation=relation, vital_status=vital_status)

        if member:
            return Response(m_serializer.data)

        return Response(m_serializer.errors)

    @action(methods=['GET'], detail=False)
    def get_members(self, request, *args, **kwargs):
        
        data = request.data
        user_id = data.get('user_id')

        member = Member.objects.filter(user_id=user_id).all().order_by('relation')
        serializer_class = MemberSerializer(member, many=True)
        
        return Response(serializer_class.data)

