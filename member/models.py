from django.db import models
from account.models import Account

# Create your models here.


class Member(models.Model):

    # RELATION_CHOICES = (
    #     ('0', 'ME'),
    #     ('1', 'FATHER'),
    #     ('2', 'MOTHER'),
    #     ('3', 'BROTHER'),
    #     ('4', 'SISTER'),
    #     ('5', 'SON'),
    #     ('6', 'DAUGHTER'),
    #     ('7', 'SPOUSE'),
    #     ('8', 'PATERNAL GRANDFATHER'),
    #     ('9', 'PATERNAL GRANDMOTHER'),
    #     ('10', 'MATERNAL GRANDFATHER'),
    #     ('11', 'MATERNAL GRANDMOTHER'),
    #     ('12', 'FATHER IN LAW'),
    #     ('13', 'MOTHER IN LAW'),
    #     ('14', 'GRANDCHILD'),
    #     ('15', 'UNCLE'),
    #     ('16', 'AUNT')
    # )

    # VITAL_STATUS = (
    #     ('0', 'ALIVE'),
    #     ('1', 'DECEASED')
    # )

    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=60, blank=True)
    last_name = models.CharField(max_length=60, blank=True)
    dob = models.DateField(null=True, blank=True)
    relation = models.CharField(
        max_length=20, default='me')
    vital_status = models.CharField(
        max_length=10)

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name = 'Family Member'
