from django.urls import path
from account.api.views import (
    api_get_user,
    api_update_user,
    api_register_user,
    ChangePasswordView
)

app_name = 'account'

urlpatterns = [
    path('register/', api_register_user, name="register"),
    path('get_user/', api_get_user, name="user"),
    path('<int:pk>/update/', api_update_user, name="update"),
    path('changepassword/', ChangePasswordView.as_view(), name="changepassword")
]