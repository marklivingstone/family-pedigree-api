from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.generics import UpdateAPIView

from account.models import Account

from account.api.serializers import AccountSerializer, AccountRegisterSerializer, ChangePasswordSerializer


@api_view(['GET', ])
def api_get_user(request):

    data = request.data
    username = data.get('username')
    user = Account.objects.filter(
        username=username
    ).order_by('pk').first()

    try:
        user = Account.objects.get(id=user.id)
    except Account.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = AccountSerializer(user)
        return Response(serializer.data)


@api_view(['POST', ])
def api_register_user(request):

    if request.method == 'POST':
        serializer = AccountRegisterSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['id'] = account.id
            data['email'] = account.email
            data['username'] = account.username
            data['first_name'] = account.first_name
            data['last_name'] = account.last_name
            data['dob'] = account.dob
            data['success'] = "Account has been successfully created."
        else:
            data = serializer.errors
        return Response(data)


@api_view(['PUT', ])
def api_update_user(request, pk):

    try:
        user = Account.objects.get(id=pk)
    except Account.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "PUT":
        serializer = AccountSerializer(user, data=request.data)
        data = {}

        if serializer.is_valid():
            serializer.save()
            data["success"] = "Account has been successfully updated"
            return Response(data=data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ChangePasswordView(UpdateAPIView):

    serializer_class = ChangePasswordSerializer
    model = Account
    
    def get_object(self, queryset=None):
        obj = self.request.user
        return obj
    
    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"response": "Old Password is Incorrect. Please try again."}, status=status.HTTP_400_BAD_REQUEST)

            new_password = serializer.data.get("new_password")
            confirm_new_password = serializer.data.get("confirm_new_password")

            if new_password != confirm_new_password:
                return Response({"response": "Passwords must match"}, status=status.HTTP_400_BAD_REQUEST)

            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response({"response": "Your password has been changed successfully"}, status=status.HTTP_200_OK)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)